opam-version: "2.0"
maintainer: [
  "Louis Gesbert <louis.gesbert@ocamlpro.com>"
  "Anil Madhavapeddy <anil@recoil.org>"
]
authors: [
  "Louis Gesbert <louis.gesbert@ocamlpro.com>"
  "Anil Madhavapeddy <anil@recoil.org>"
]
homepage: "https://github.com/ocaml/opam-depext"
bug-reports: "https://github.com/ocaml/opam-depext/issues"
license: "LGPL-2.1 with OCaml linking exception"
dev-repo: "git+https://github.com/ocaml/opam-depext.git#2.0"
build: [make]
available: opam-version >= "2.0.0~beta5"
synopsis: "Query and install external dependencies of OPAM packages"
description: """
opam-depext is a simple program intended to facilitate the interaction between
OPAM packages and the host package management system. It can query OPAM for the
right external dependencies on a set of packages, depending on the host OS, and
call the OS's package manager in the appropriate way to install them."""
depends: [
  "ocaml" {>= "4.0.0"}
]
flags: plugin
url {
  src:
    "https://github.com/ocaml/opam-depext/releases/download/v1.1.2/opam-depext-full-1.1.2.tbz"
  checksum: [
    "md5=d71c9c0ada811ccf0669d09e1b0329da"
    "sha256=39e11dee865cd39e513dee01f31ce38a11583f92525c28c0a7ec34935569b9b0"
    "sha512=f02d0408040268002d246058185bd177621fe1e0fecc918161da9672e9d40107ab0a50a4758426f652d6add507f5af3c1a2bfb14daffb9353ebfeebf86a2323a"
  ]
}
