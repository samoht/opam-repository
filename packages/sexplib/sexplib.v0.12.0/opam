opam-version: "2.0"
maintainer: "opensource@janestreet.com"
authors: ["Jane Street Group, LLC <opensource@janestreet.com>"]
homepage: "https://github.com/janestreet/sexplib"
bug-reports: "https://github.com/janestreet/sexplib/issues"
dev-repo: "git+https://github.com/janestreet/sexplib.git"
doc: "https://ocaml.janestreet.com/ocaml-core/latest/doc/sexplib/index.html"
license: "MIT"
build: [
  ["dune" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml"    {>= "4.04.2"}
  "parsexp"  {>= "v0.12" & < "v0.13"}
  "sexplib0" {>= "v0.12" & < "v0.13"}
  "dune"     {build & >= "1.5.1"}
  "num"
]
synopsis: "Library for serializing OCaml values to and from S-expressions"
description: "
Part of Jane Street's Core library
The Core suite of libraries is an industrial strength alternative to
OCaml's standard library that was developed by Jane Street, the
largest industrial user of OCaml.
"
url {
  src:
    "https://ocaml.janestreet.com/ocaml-core/v0.12/files/sexplib-v0.12.0.tar.gz"
  checksum: [
    "md5=a7f9f8a414aed6cc56901199cda020f6"
    "sha256=c54b11b7d30eb4d6587834f9011b37f94619b76cdc496ea22633079ced59827f"
    "sha512=bd050e59f5269f15b3362891f98417c78bbe6e18c630488ac3df769dd70180beb4e1bbf55e32327fd2dec9a6041969bcaa4f9d16b9295e33cc82af1515404701"
  ]
}
